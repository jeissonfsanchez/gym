<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MigracionTablas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Migración de las tablas y sus Lláves Foráneas */

        Schema::create("usuarios", function ($tabla){
            $tabla->id();
            $tabla->enum("tipo",["USUARIO","EMPLEADO","ADMIN"])->default("USUARIO");
            $tabla->string("nombre",100);
            $tabla->string("identificacion",100);
            $tabla->string("movil",50);
            $tabla->string("correo",100)->unique();
            $tabla->string("pass_encrypt");
            $tabla->string("pass_decrypt",50);
            $tabla->enum("estado",["ACTIVO","INACTIVO"])->default("ACTIVO");
            $tabla->dateTime("fecha_registro")->useCurrent();
            $tabla->dateTime("fecha_actualizacion")->nullable()->useCurrentOnUpdate();
        });

        Schema::create('sedes', function (Blueprint $tabla) {
            $tabla->id();
            $tabla->string("nombre",100);
            $tabla->integer("capacidad");
            $tabla->enum("estado",["ACTIVO","INACTIVO"])->default("ACTIVO");
            $tabla->dateTime("fecha_creacion")->useCurrent();
            $tabla->dateTime("fecha_actualizacion")->nullable()->useCurrentOnUpdate();
        });

        Schema::create('reservas',function (Blueprint $tabla){
            $tabla->id();
            $tabla->unsignedBigInteger("id_sede");
            $tabla->unsignedBigInteger("id_usuario");
            $tabla->date("fecha");
            $tabla->enum("franja",["DIA","TARDE","NOCHE"])->default("DIA");
            $tabla->enum("estado",["ACTIVO","INACTIVO"])->default("ACTIVO");
            $tabla->dateTime("fecha_creacion")->useCurrent();
            $tabla->dateTime("fecha_actualizacion")->nullable()->useCurrentOnUpdate();
            //Llave Foránea
            $tabla->foreign("id_sede")->references("id")->on("sedes");
            $tabla->foreign("id_usuario")->references("id")->on("usuarios");
        });

        Schema::create('tokens', function (Blueprint $tabla) {
            $tabla->id();
            $tabla->unsignedBigInteger("id_usuario");
            $tabla->longText("token");
            $tabla->dateTime("fecha_creacion")->useCurrent();
            //Llave Foránea
            $tabla->foreign("id_usuario")->references("id")->on("usuarios");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /* Inversa de la tablas (de la más reciente a la primera) */

        Schema::table("tokens", function (Blueprint $tabla){
            $tabla->dropForeign(["id_usuario"]);
            $tabla->dropIfExists("tokens");
        });

        Schema::table("reservas", function (Blueprint $tabla){
            $tabla->dropForeign(["id_usuario"]);
            $tabla->dropForeign(["id_sede"]);
            $tabla->dropIfExists("reservas");
        });


        Schema::dropIfExists("sedes");


        Schema::dropIfExists("usuarios");
    }
}
