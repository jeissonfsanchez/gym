<?php

namespace Database\Factories\Usuario;

use App\Models\Usuario\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class UsuarioFactory extends Factory
{
    protected $contadorUsuario = 0;

    protected $contadorEmpleado = 0;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Usuario::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(){

        if ($this->contadorUsuario < 5){
            $tipo = 'USUARIO';
            $this->contadorUsuario++;
        }
        elseif ($this->contadorEmpleado < 2){
            $tipo = 'EMPLEADO';
            $this->contadorEmpleado++;
        }
        else{
            $tipo = 'ADMIN';
            $this->contadorUsuario = 0;
            $this->contadorEmpleado = 0;
        }

        $strPass = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789*.";
        $password = substr( str_shuffle($strPass), 0, 8 );
        return [
            "tipo" => $tipo,
            "nombre" => $this->faker->name,
            "identificacion" => $this->faker->numberBetween(100000,2000000000),
            "movil" => $this->faker->numberBetween(3000000000,3300000000),
            "correo" => $this->faker->safeEmail,
            "pass_encrypt" => Hash::make($password),
            "pass_decrypt" => $password
        ];
    }
}
