<?php

namespace Database\Factories\Gimnasio;
use App\Models\Gimnasio\Reserva;
use App\Models\Gimnasio\Sede;
use App\Models\Usuario\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReservaFactory extends Factory
{

    protected $idUsuarioLista = [];

    protected $idSedeLista = [];

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reserva::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        if (empty($this->idUsuarioLista)) {
            $i = 0;
            $lector = Usuario::select('id')->where('tipo','USUARIO')->get();
            foreach ($lector as $a) {
                $this->idUsuarioLista[$i] = $a->id;
                $i++;
            }
        }
        $idUsuario = $this->faker->randomElement($this->idUsuarioLista);



        if (empty($this->idSedeLista)) {
            $i = 0;
            $sede = Sede::all();
            foreach ($sede as $a) {
                $this->idSedeLista[$i] = $a->id;
                $i++;
            }
        }
        $idSede = $this->faker->randomElement($this->idSedeLista);

        $this->idUsuarioLista = array_diff($this->idUsuarioLista,[$idUsuario]);

        return [
            "id_sede" => $idSede,
            "id_usuario" => $idUsuario,
            "fecha" => now(),
            "franja" => $this->faker->randomElement(["DIA","TARDE","NOCHE"])

        ];
    }
}
