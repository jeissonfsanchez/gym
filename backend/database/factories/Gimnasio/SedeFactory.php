<?php

namespace Database\Factories\Gimnasio;

use App\Models\Gimnasio\Sede;
use App\Models\Usuario\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

class SedeFactory extends Factory
{
    protected $idListaEmpleado = [];

    protected $idListaLibreria = [];
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sede::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "nombre"=>$this->faker->company(),
            "capacidad"=>$this->faker->numberBetween(3,6)
        ];
    }
}
