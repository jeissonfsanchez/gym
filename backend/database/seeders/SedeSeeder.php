<?php

namespace Database\Seeders;


use App\Models\Gimnasio\Sede;
use Illuminate\Database\Seeder;

class SedeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sede::factory(2)->create();
    }
}
