<?php

namespace Tests\Feature;

use App\Models\Usuario\Token;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class loginTest extends TestCase
{
    /*
     * Credenciales para poder correr los test
     * NOTA: El correo debe ser real pues será validado
     * y posterior la plataforma enviará correos.
     */

    protected $correo = "jf_328@hotmail.com";
    protected $password = "Test1234*";


    /** * @test */
    function estadoAPI()
    {
        $response = $this->get('api/usuario');
        $response->assertStatus(200);
        $response->assertSee('API Funcionando');

    }

    /** * @test */
    function crearUsuario(){
        $response = $this->post('api/usuario/registro',[
            "nombre" => "Jeisson Sanchez",
            "identificacion" => "10101010",
            "movil" => "3022760419",
            "correo" => $this->correo,
            "password" => $this->password,
            "tipo" => "EMPLEADO"
        ]);
        $response->assertStatus(200);
    }

    /** * @test */
    function loginUsuario(){
        $response = $this->post('api/usuario/login',[
            "correo" => $this->correo,
            "password" => $this->password
        ]);
        $response->assertStatus(200);
    }

    /** * @test */
    function recordarPassword(){
        $response = $this->post('api/usuario/recordar',[
            "correo" => $this->correo
        ]);
        $response->assertStatus(200);
    }

    /** * @test */
    function cambioPassword(){

        /* Para asignar una nueva password aleatoria */
        $strPass = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789*.";
        $nuevaPassword = substr( str_shuffle($strPass), 0, 8 );

        $infoUsuario = Usuario::where('correo',$this->correo)->first();

        $infoToken = Token::where('id_usuario',$infoUsuario->id)->orderBy('fecha_creacion','DESC')->first();
        $response = $this->post('api/usuario/cambiarPassword',[
            "token" => $infoToken->token,
            "password" => $nuevaPassword
        ]);

        $response->assertStatus(200);
    }

}
