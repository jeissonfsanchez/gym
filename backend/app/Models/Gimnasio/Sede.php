<?php

namespace App\Models\Gimnasio;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sede extends Model
{
    use HasFactory;

    protected $model = Sede::class;

    protected $table = "sedes";

    public $timestamps = false;

    protected $fillable = [
        "nombre","capacidad","estado"
    ];

    protected $guarded = [
        "id","fecha_creacion"
    ];

    protected $hidden = [
        "fecha_actualizacion"
    ];
}
