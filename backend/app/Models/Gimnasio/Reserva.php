<?php

namespace App\Models\Gimnasio;

use App\Models\Autor\Libro;
use Database\Factories\Gimnasio\ReservaFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    use HasFactory;

    protected $table = "reservas";

    protected $model = Reserva::class;

    public $timestamps = false;

    protected $fillable = [
        "id_sede","id_usuario","fecha","franja","estado"
    ];

    protected $guarded = [
        "id","fecha_creacion"
    ];

    protected $hidden = [
        "fecha_actualizacion"
    ];

    protected static function newFactory()
    {
        return ReservaFactory::new();
    }

    public function dataLibro(){
        return $this->hasOne(Libro::class,'id','id_libro');
    }
}
