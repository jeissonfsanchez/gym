<?php

namespace App\Models\Usuario;

use Database\Factories\Usuario\UsuarioFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Tymon\JWTAuth\Contracts\JWTSubject as JWT;
use Illuminate\Foundation\Auth\User as Autenticacion;

class Usuario extends Autenticacion implements JWT
{
    use HasFactory;

    protected $model = Usuario::class;

    protected $table = "usuarios";

    public $timestamps = false;

    protected $fillable = [
        "nombre","correo","identificacion","movil","pass_encrypt","pass_decrypt","tipo","estado"
    ];

    protected $guarded = [
        "id"
    ];

    protected $hidden = [
        "pass_encrypt","pass_decrypt","fecha_creacion","fecha_actualizacion"
    ];

    protected static function newFactory()
    {
        return UsuarioFactory::new();
    }

    public function  getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

}
