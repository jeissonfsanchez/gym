<?php

namespace App\Models\Usuario;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    use HasFactory;

    protected $model = Token::class;

    protected $table = "tokens";

    public $timestamps = false;

    protected $fillable = [
        "id_usuario","token","estado","fecha_creacion"
    ];

    protected $guarded = [
        "id"
    ];

    protected $hidden = [
        "fecha_creacion","fecha_actualizacion"
    ];

}
