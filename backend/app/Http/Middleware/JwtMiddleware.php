<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Exception;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;


class JwtMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof TokenInvalidException){
                return response()->json(['success'=> false, 'message' => 'Token inválido'],406);
            }else if ($e instanceof TokenExpiredException){
                return response()->json(['success'=> false, 'message' => 'Token Expirado'],406);
            }else{
                return response()->json(['success'=> false, 'message' => 'No tiene autorización'],401);
            }
        }
        return $next($request);
    }
}
