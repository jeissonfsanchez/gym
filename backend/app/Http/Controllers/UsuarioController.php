<?php

namespace App\Http\Controllers;

use App\Mail\Correo;
use App\Models\Usuario\Token;
use App\Models\Usuario\Usuario;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UsuarioController extends Controller
{
    public function registro(Request $request): JsonResponse
    {
        $validador = Validator::make($request->all(),[
            'nombre' => 'required|regex:/^[a-zA-Z\s]{3,200}$/',
            'identificacion' => 'required|regex:/^[a-zA-Z0-9]{3,200}$/',
            'movil' => 'required|regex:/^[0-9]{3,200}$/',
            'correo' => 'required|email|max:200|unique:usuarios',
            'password' => 'required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*&.])[A-Za-z\d@$!%*&.]{6,50}$/',
            'tipo' => 'required'
        ]);

        if ($validador->fails()){
            return response()->json(['success'=>false,'message'=>['validacion'=>$validador->errors()->keys()]],406);
        }
        list($usuario, $dominio) = explode('@', $request->correo);
        $banCorreo = checkdnsrr($dominio);
        if ($banCorreo){
            $usuario = Usuario::create(
                array_merge(
                    $validador->validate(), [
                        "pass_encrypt" => Hash::make($request->password),
                        "pass_decrypt" => $request->password,
                    ]
                ));
        }else{
            return response()->json(['success'=>false,'message'=>'Favor ingresar un correo válido.'],406);
        }

        return response()->json([
            'message' => 'Usuario creado correctamente',
            'success' => true,
            'datos' => $usuario
        ],200);
    }

    public function login(Request $request): JsonResponse
    {
        $user = Usuario::where([
            'correo' => $request->correo,
            'tipo' => 'EMPLEADO'
        ])->first();

        if (!$user ){
            return response()->json([ 'message' => 'Error en la credenciales de acceso.', 'success' => false], 401);
        }else{
            $banPass = (Hash::check($request->password, $user->pass_encrypt));
        }

        if (!$banPass){
            return response()->json([ 'message' => 'Error en la credenciales de acceso.', 'success' => false], 401);
        }elseif (!$token = auth()->login($user)) {
            return response()->json([ 'message' => 'No tiene autorización para acceder a la plataforma.', 'success' => false], 401);
        }

        return $this->asignarToken($token);
    }

    protected function asignarToken($token): JsonResponse
    {
        return response()->json([
            'success'=>true,
            'message'=>'Acceso concedido',
            'token' => $token,
            'tipo' => 'bearer',
            'expira_en' => auth('api')->factory()->getTTL() * 5
        ],200);
    }

    public function recordar(Request $request): JsonResponse
    {
        $validadorCorreo = Validator::make($request->all(),[
            'correo' => 'required|email|max:200'
        ]);

        if ($validadorCorreo->fails()){
            return response()->json(['success'=>false,'message'=>['validacion'=>$validadorCorreo->errors()->keys()]],406);
        }

        $banRecordar = false;
        $menRecordar = "Si el correo ingresado es correcto, se le enviará un mail con los pasos a seguir";

        $usuario = Usuario::where('correo',$request->correo)->first();

        $strToken = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789*.";
        $tokenNoCrypt = substr( str_shuffle($strToken), 0, 20 );
        $tokenCrypt = hash('sha256', $tokenNoCrypt);

        Token::create([
            "id_usuario" => $usuario->id,
            "token" => $tokenCrypt
        ]);

        if ($usuario != null){
                $nombre = $usuario->nombre;
                $correo = $usuario->correo;
                $link = 'http://localhost:4200/usuario/confirmar/?token='.$tokenCrypt;
                $details = ['nombre'=>$nombre,'link'=>$link,'vista'=>'recordar_usuario'];
                Mail::to($correo)->send(new Correo($details));
                $banRecordar = true;
        }

        return response()->json([
            'message' => $menRecordar,
            'success' => $banRecordar
        ],200);
    }

    public function cambiarPassword(Request $request): JsonResponse
    {
        $banRecordar = false;
        $menRecordar = "Token vencido.";
        $estado = 401;

        $infoToken = Token::where('token',$request->token)->first();

        if ($infoToken == null){
            return response()->json(['success'=>false,'message'=>"Token errado."],$estado);
        }
        $dateConsulta = Carbon::parse($infoToken->fecha_creacion);

        $diffMin = $dateConsulta->diffInMinutes(now());

        if ($diffMin <= 20) {

            $usuario = Usuario::where('id', $infoToken->id_usuario)->first();

            if ($usuario != null) {

                $nombre = $usuario->nombre;
                $correo = $usuario->correo;
                $usuario->update([
                    "pass_encrypt" => Hash::make($request->password),
                    "pass_decrypt" => $request->password
                ]);

                $details = ['nombre' => $nombre, 'vista' => 'nueva_password'];
                Mail::to($correo)->send(new Correo($details));

                $banRecordar = true;
                $menRecordar = "Contraseña cambiada correctamente.";
                $estado = 200;
            }
        }

        return response()->json([
            'message' => $menRecordar,
            'success' => $banRecordar
        ],$estado);
    }

    public function index(){
        $user[0] = auth()->user()->toArray();
        return response()->json(["success"=>true,"message"=>$user]);
    }


}
