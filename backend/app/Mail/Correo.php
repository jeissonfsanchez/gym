<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Correo extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    public function __construct($details)
    {
        $this->details = $details;
    }

    public function build()
    {

        if ($this->details["vista"] == 'recordar_usuario')
        {
        $correo = $this->subject("Solicitud nueva contraseña")
            ->view('emails.mailRecordar');
        }
        elseif ($this->details["vista"] == 'nueva_password')
        {
            $correo = $this->subject("Confirmación cambio contraseña")
                ->view('emails.mailConfirmacionCambio');
        }
        else{
            $correo = 'sin enviar notificación';
        }

        return $correo;
    }
}
