<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

# Gym

API REST (login sencillo con auth segura) para el test code bodytech

## Introducción

Este proyecto se divide en frontend y backend

Para poder correr los aplicativos de forma correcta, es necesario tener en cuenta lo siguiente:

## Backend (Laravel v8)

En el backend favor colocar las credenciales enviadas en el archivo .env.example así como configurar la bd (por defecto se llama backend) con las respectivas credenciales e BD.

Una vez configurado esto, favor correr composer install. Esto hará que se instale lo requerido, luego de ello, corre la migración y por último el seeder.

Una vez termine el proceso, iniciar el servidor con php artisan serve (e iniciará la API)


Para realizar las pruebas, favor ejecutar el archivo tests/Feature/loginTest.php, este ejecutará:

1) Validará el servicio funcional de la API.
2) La creación del usuario
3) Se logueará el usuario
4) Hará el proceso de recordar contraseña
5) Cambiará la contraseña (Con el token asignado)

Dentro del archivo nombrado están las variables reservadas de correo y password para poder testear. Es recomendable que se realice con un correo real, pues éste se validará.


## Aclaración.

Se solicitó un login completo y se añadió el acceso del usuario a loguear (solo empleado) con un token (JWT) que expira en 5 minutos. Posterior a ello, si siguen navegando, será redirigido al index.
