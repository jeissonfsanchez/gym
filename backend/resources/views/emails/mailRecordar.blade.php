<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table style='padding:3%; background: #fff;width:100%; box-sizing: border-box !important;'>
    <tr>
        <td style='float:left; background: #fff; padding: 0px 5%; margin:auto'>
            <div>
                <p>
                    Hola <b>{{ $details['nombre'] }}</b>
                </p>
                <p>
                    Hemos recibido una solicitud de su parte para generar una nueva contraseña.
                </p>
                <p>
                    Favor dar <a href="{{ $details['link'] }}">clic aquí.</a> y seguir las instrucciones.</p>
                <p>
                    En caso que el link no lo redirija, favor copiar la siguiente url {{ $details['link'] }}, ponerla en el navegador y teclear enter.
                </p>
                <p>
                    Le recordamos que este proceso tiene una vigencia de 20 minutos una vez ha solicitado la solicitud de cambiar la contraseña.
                </p>
            </div>
        </td>
    </tr>
</table>

<table style='padding:3%;background: #fff;width:100%;border-spacing:0px 0px; display:none'>
    <tr>
        <td style='width: 80%; padding: 2% 5%;margin:auto;'></td>
    </tr>
</table>

<table style='padding:3%;background: #fff;width:100%;border-spacing:0px 0px;'>
    <tr>
        <td style='width: 80%; padding: 2% 5%;margin:auto; background: #172184; color: #fff;'>
            <p style='font-size:12px;'>
                Atentamente,<br><br>Equipo de soporte
            </p>
        </td>
    </tr>
</table>


</body>
</html>


