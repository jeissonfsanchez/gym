<?php

use App\Http\Controllers\UsuarioController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => ['jwt.verify'],
    'prefix' => 'portal'
], function () {
    Route::post('', [UsuarioController::class,'index']);
});
Route::prefix('usuario')->group(function () {
    Route::get('',function (){ return "API Funcionando";});
    Route::post('login', [UsuarioController::class,'login']);
    Route::post('registro',[ UsuarioController::class, 'registro']);
    Route::post('recordar',[ UsuarioController::class, 'recordar']);
    Route::post('cambiarPassword',[ UsuarioController::class, 'cambiarPassword']);
});


