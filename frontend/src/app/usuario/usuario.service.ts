import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {  Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private apiURL = 'http://localhost:8000/api/usuario/';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  crearUsuario(usuario): Observable<Usuario> {
    return this.httpClient.post<Usuario>(this.apiURL + 'registro/', JSON.stringify(usuario), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  login(usuario): Observable<Usuario> {
    return this.httpClient.post<Usuario>(this.apiURL + 'login/', JSON.stringify(usuario), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  recordarUsuario(usuario): Observable<Usuario> {
    return this.httpClient.post<Usuario>(this.apiURL + 'recordar/', JSON.stringify(usuario), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  cambioPassword(usuario): Observable<Usuario> {
    let data = { token: sessionStorage.tokenCambioPass, password: usuario.password };
    return this.httpClient.post<Usuario>(this.apiURL + 'cambiarPassword/', JSON.stringify(data), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  // tslint:disable-next-line:typedef
  errorHandler(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código de error: ${error.status}\nMensaje: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}
