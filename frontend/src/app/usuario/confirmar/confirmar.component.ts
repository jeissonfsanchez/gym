import {Component, OnInit} from '@angular/core';
import {UsuarioService} from '../usuario.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './confirmar.component.html',
  styleUrls: ['./confirmar.component.css']
})
export class ConfirmarComponent implements OnInit {

  form: FormGroup;

  constructor(
    public usuarioService: UsuarioService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      password:  new FormControl('', [ Validators.required, Validators.pattern('^[0-9a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÑñüÜ*.-_ \-\']+') ])
    });

    let url_string = window.location.href;
    let url = new URL(url_string);
    let token = url.searchParams.get("token");
    if (token != null){
      sessionStorage.tokenCambioPass = token;
    }else{
      this.router.navigateByUrl('usuario/index');
    }
  }

  // tslint:disable-next-line:typedef
  get f(){
    return this.form.controls;
  }

  // tslint:disable-next-line:typedef
  submit(){
    this.usuarioService.cambioPassword(this.form.value).subscribe((resultado) => {
      alert(resultado.message);
      if (resultado.success) {
        this.router.navigateByUrl('usuario/index');
      }else{
        this.router.navigateByUrl('usuario/create');
      }
    });
  }

}

