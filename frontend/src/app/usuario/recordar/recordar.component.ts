import {Component, OnInit} from '@angular/core';
import {UsuarioService} from '../usuario.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './recordar.component.html',
  styleUrls: ['./recordar.component.css']
})
export class RecordarComponent implements OnInit {

  form: FormGroup;

  constructor(
    public usuarioService: UsuarioService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      correo: new FormControl('', [ Validators.required, Validators.email ])
    });
  }

  // tslint:disable-next-line:typedef
  get f(){
    return this.form.controls;
  }

  // tslint:disable-next-line:typedef
  submit(){
    this.usuarioService.recordarUsuario(this.form.value).subscribe((resultado) => {
      alert(resultado.message);
      if (resultado.success) {
        this.router.navigateByUrl('usuario/index');
      }else{
        this.router.navigateByUrl('usuario/recordar');
      }
    });
  }

}

