import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexComponent } from './index/index.component';
import { CreateComponent } from './create/create.component';
import { RecordarComponent } from './recordar/recordar.component';
import { ConfirmarComponent } from './confirmar/confirmar.component';

const routes: Routes = [
  { path: 'usuario', redirectTo: 'usuario/index', pathMatch: 'full'},
  { path: 'usuario/index', component: IndexComponent },
  { path: 'usuario/create', component: CreateComponent },
  { path: 'usuario/recordar', component: RecordarComponent },
  { path: 'usuario/confirmar', component: ConfirmarComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
