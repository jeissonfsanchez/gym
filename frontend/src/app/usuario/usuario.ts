export interface Usuario {
  success: any;
  message: any;
  id: number;
  nombre: string;
  correo: string;
  tipo: number;
  identificacion: string;
  movil: string;
  token: any;
}
