import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {  Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Empleado } from './empleado';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  private apiURL = 'http://localhost:8000/api/portal/';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  listar(): Observable<Empleado[]> {
    const dato = JSON.stringify({token: sessionStorage.token});
    return this.httpClient.post<Empleado[]>(this.apiURL, dato, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  // tslint:disable-next-line:typedef
  borrarLibro(id){
    return this.httpClient.delete<Empleado>(this.apiURL +'libros/borrar/' + id, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  completarDonacion(id_libro){
    const dato = JSON.stringify({id_lector: sessionStorage.id});
    return this.httpClient.post<Empleado>(this.apiURL + 'nueva_idea/donar/' + id_libro, dato, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  // tslint:disable-next-line:typedef
  errorHandler(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código de error: ${error.status}\nMensaje: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}
