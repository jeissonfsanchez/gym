import { Component, OnInit } from '@angular/core';

import { EmpleadoService } from '../empleado.service';
import { Empleado } from '../empleado';
import {Router} from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  empleados: Empleado[] = [];
  constructor(public empleadoService: EmpleadoService, private router: Router) { }

  ngOnInit(): void {

    this.empleadoService.listar().subscribe((data: Empleado[]) => {
      let datos: any = {};
      datos = data;
      if (datos.success){
        if (datos.message[0].tipo == 'EMPLEADO') {
          this.empleados = datos.message;
        }else{
          this.router.navigateByUrl('usuario/index');
        }
      }else{
        this.router.navigateByUrl('usuario/index');
      }
    });
  }

  // tslint:disable-next-line:typedef
  mensaje(){
    alert("Este sitio está en construcción. Recuerde que el token expira en 5 minutos y una vez pasado ese tiempo. La plataforma cerrará sesión");
    this.ngOnInit();
  }

}
