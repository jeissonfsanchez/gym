# Gym

API REST (login sencillo con auth segura) para el test code bodytech

## Introducción

Este proyecto se divide en frontend y backend

Para poder correr los aplicativos de forma correcta, es necesario tener en cuenta lo siguiente:

##Frontend (Angular v10)

Correr npm install y una vez acabe, correr ng serve.

## Aclaración.

Se solicitó un login completo y se añadió el acceso del usuario a loguear (solo empleado) con un token (JWT) que expira en 5 minutos. Posterior a ello, si siguen navegando, será redirigido al index.
